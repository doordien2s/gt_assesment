
resource "tls_private_key" "tls_for_ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "ssh_generated_key" {
  key_name   = "${var.default_app_name}-sshkey" 
  public_key = tls_private_key.tls_for_ssh_key.public_key_openssh
}

resource "aws_secretsmanager_secret" "ssh_private" {
  name = "${var.default_app_name}-${var.secret_keyname}"
}

resource "aws_secretsmanager_secret_version" "ssh_private_in" {
  secret_id     = aws_secretsmanager_secret.ssh_private.id
  secret_string = tls_private_key.tls_for_ssh_key.private_key_pem
}