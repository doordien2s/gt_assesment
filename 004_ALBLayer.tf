

resource "aws_lb" "alb_app" {
  name               = "${var.default_app_name}-public-alb"
  internal           = false
  load_balancer_type = "application"
  subnets             = [aws_subnet.public_subnet.0.id,aws_subnet.public_subnet.1.id,aws_subnet.public_subnet.2.id]
  ip_address_type    = "ipv4"
  security_groups    = [aws_security_group.lb_sg.id]

  enable_cross_zone_load_balancing = true

  tags = {
    Name = "${var.default_app_name}-public-alb"
  }
}




resource "aws_alb_target_group" "alb_app_tg" {
  name     = "${var.default_app_name}-app-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.main_vpc.id}"
  

  # Alter the destination of the health check to be the login page.
  health_check {
    path = "/"
    port = 80
    protocol = "HTTP"
    matcher = "200"
    timeout = 5
    unhealthy_threshold = 3
  }
  tags = {
    Name = "${var.default_app_name}-app-tg"
  }
}

resource "aws_alb_listener" "listener_http" {
  load_balancer_arn = aws_lb.alb_app.arn
  port              = "80"
  protocol          = "HTTP"

 default_action {   
    type             = "forward"
    target_group_arn = aws_alb_target_group.alb_app_tg.arn
    
  
  }  
  
}


