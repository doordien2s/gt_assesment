

resource "aws_launch_configuration" "alc" {
  name_prefix     = "${var.default_app_name}-app-ec2"
  image_id        = data.aws_ami.amazon-linux.id
  instance_type   = "t2.micro"
  user_data       = file("scripts/userdata.sh")
  security_groups = [aws_security_group.instance_sg.id]
  key_name = aws_key_pair.ssh_generated_key.key_name

  lifecycle {
    create_before_destroy = true  
  }
}


resource "aws_autoscaling_group" "asg" {
  name = "${var.default_app_name}-asg"
  min_size             = 1
  max_size             = 2
  desired_capacity     = 1
  launch_configuration = aws_launch_configuration.alc.name
  vpc_zone_identifier  = [aws_subnet.app_subnet.0.id,aws_subnet.app_subnet.1.id,aws_subnet.app_subnet.2.id]
     lifecycle { 
    ignore_changes = [desired_capacity, target_group_arns]

  }
}

# Create a new ALB Target Group attachment
resource "aws_autoscaling_attachment" "asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.asg.id
  lb_target_group_arn    = aws_alb_target_group.alb_app_tg.arn
}
 
