#!/bin/bash
sudo yum update -y
sudo amazon-linux-extras install nginx1 -y

sudo cat > /usr/share/nginx/html/apiResponse.json << EOF


{
        "ip_address":"192.168.0.0",
        "subnet_size":"/16"
}

EOF

sudo cat >  /etc/nginx/default.d/api.conf << EOF

        location /vendor_ip
        {
        default_type application/json;
        index apiResponse.json;
        alias /usr/share/nginx/html;
        }

EOF
sudo systemctl enable nginx
sudo systemctl restart nginx