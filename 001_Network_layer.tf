data "aws_availability_zones" "available" {}

resource "aws_vpc" "main_vpc" {
  cidr_block           = "10.10.0.0/16"
  enable_dns_hostnames = true
  tags = {
    Name = "${var.default_app_name}-Main-VPC"
  }
}

resource "aws_subnet" "public_subnet" {
  count = "${length(data.aws_availability_zones.available.names)}"
  vpc_id = "${aws_vpc.main_vpc.id}"
  cidr_block = "10.10.${10+count.index}.0/24"
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  
  tags={
    Name = "${var.default_app_name}-PublicSubnet-${count.index + 1}"
  }
}
resource "aws_subnet" "app_subnet" {
  count = "${length(data.aws_availability_zones.available.names)}"
  vpc_id = "${aws_vpc.main_vpc.id}"
  cidr_block = "10.10.${20+count.index}.0/24"
  availability_zone= "${data.aws_availability_zones.available.names[count.index]}"
  map_public_ip_on_launch = false
  tags={
    Name = "${var.default_app_name}-AppSubnet-${count.index + 1}"
  }
}


resource "aws_internet_gateway" "igw" {
 vpc_id = aws_vpc.main_vpc.id
 
 tags = {
   Name = "${var.default_app_name}-Internet-GateWay"
 }
}

resource "aws_eip" "nat_eip" {
  domain                       = "vpc"
  depends_on                = [aws_internet_gateway.igw]
}

resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public_subnet[0].id

  tags = {
    Name = "${var.default_app_name}-Nat-GateWay"
  }
  depends_on = [aws_eip.nat_eip]
}


resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "${var.default_app_name}-Public-route-table"
  }
}
resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "${var.default_app_name}-private-route-table"
  }
}

resource "aws_route" "public_internet_igw_route" {
  route_table_id         = aws_route_table.public_route_table.id
  gateway_id             = aws_internet_gateway.igw.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_route" "nat_ngw_route" {
  route_table_id         = aws_route_table.private_route_table.id
  nat_gateway_id         = aws_nat_gateway.ngw.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_route_table_association" "public_route_association" {
  count = length(aws_subnet.public_subnet)
  route_table_id = aws_route_table.public_route_table.id
  subnet_id      = element(aws_subnet.public_subnet[*].id, count.index)
}


resource "aws_route_table_association" "private_route_association" {
  count = length(aws_subnet.app_subnet)
  route_table_id = aws_route_table.private_route_table.id
  subnet_id      = element(aws_subnet.app_subnet[*].id, count.index)
}


